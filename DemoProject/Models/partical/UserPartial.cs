﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DemoProject.Models
{
    [MetadataType(typeof(UserMetadata))]
    public partial class User
    {
    }

    public class UserMetadata
    {
        [DisplayName("姓名")]
        [Required]
        [StringLength(5)]
        public string Name { get; set; }

        [Required]
        [StringLength(100)]
        [EmailAddress]
        public string Email { get; set; }

        [DisplayName("密碼")]
        [Required]
        [StringLength(10)]
        public string Password { get; set; }

        [Required]
        [DisplayName("生日")]
        [DataType(DataType.Date)]
        public System.DateTime Birthday { get; set; }

        [Required]
        [DisplayName("性別")]
        [Range(0,1)]
        public int Sex { get; set; }
    } 

}